# opencv-training
For 3-image-filters.py, keyboard controls are:
	s - enable sepia
	i - enable invert
	r - more blue (while sepia is enabled)
	t - more red (while sepia is enabled)
	y - more green (while sepia is enabled)
