import numpy as np
import cv2
import atexit
import signal
import sys
from scipy import stats

cap = cv2.VideoCapture(0)

DESTROYED = 0

#so i can exit anytime
def fatal():
    global DESTROYED
    if(not DESTROYED):
        DESTROYED = 1
        cap.release()
        cv2.destroyAllWindows()
        print "\nDESTROYED"


def sigint(signal, frame):
    fatal()
    sys.exit(0)

atexit.register(fatal)
signal.signal(signal.SIGINT, sigint)

while(True):
    ret, frame = cap.read()
    weird = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    cv2.imshow('meh', weird)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
