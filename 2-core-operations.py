import numpy as np
import cv2
import math


def overlay(img1, img2):
    img2 = invertMe(img2)  # test
    rows, cols, channels = img2.shape
    roi = img1[0:rows, 0:cols]
    img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(img2gray, 100, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)
    img1_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)
    showSomething('a',img1_bg)
    img2_fg = cv2.bitwise_and(img2, img2, mask=mask)
    showSomething('b',img2_fg)
    dst = cv2.add(img1_bg, img2_fg)
    img1[0:rows, 0:cols] = dst
    return img1


def invertMe(img):
    img = cv2.bitwise_not(img)
    # wip
    return img


def grayscaleMe(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # wip
    return img


def hsvMe(img, b, g, r):
    # wip
    return img


def roiMe(img, y, h, x, w):
    cimg = img[y:y + h, x:x + w]
    return cimg


def roiMeSelect(img):
    bounds = cv2.selectROI(img, True)
    return bounds


def loadSomething(imgURI):
    img = cv2.imread(imgURI)
    return img

def saveMe(fname,img):
    cv2.imwrite(fname,img)


def showSomething(hwnd_name, img):
    cv2.imshow(hwnd_name, img)

if __name__ == "__main__":
    img = loadSomething('./assets/tox.jpg')
    logo = loadSomething('./assets/accenture.png')
    #logo = invertMe(logo)
    #bounds = roiMeSelect(img)
    # print bounds  #iz: x,y,w,h
    # img = roiMe(img,
    #	int(bounds[1]),
    #	int(bounds[3]),
    #	int(bounds[0]),
    #	int(bounds[2]))
    #img = hsvMe(img, 200, 0, 0)
    #img = grayscaleMe(img)
    #sum0 = cv2.addWeighted(logo, 0.4, img, 1.0, 1.0)
    #logo = cv2.resize(logo, (0,0), fx=0.25, fy=0.25)
    img = overlay(img, logo)
    showSomething('umji', img)
    #saveMe('./assets/tox-edit.jpg', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
