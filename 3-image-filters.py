import numpy as np
import cv2
import atexit
import signal
import sys
from scipy import stats

# start capture grr
cap = cv2.VideoCapture(0)
invert = False
sepia = False
moreblue = False
moregreen = False
morered = False
counter = 0
DESTROYED = 0


# so i can exit anytime
def fatal():
    global DESTROYED
    if(not DESTROYED):
        DESTROYED = 1
        #safety meh
        cap.release()
        cv2.destroyAllWindows()
        print "\nDESTROYED"


def sigint(signal, frame):
    fatal()
    sys.exit(0)

atexit.register(fatal)
signal.signal(signal.SIGINT, sigint)

def invertMe(img):
    img = cv2.bitwise_not(img)
    return img


def sepiaMe(img, intensity=0.5, moreB=False, moreG=False, moreR=False):
    #check if what channel to max out
    b, g, r = 20 if not moreB else 255, 66 if not moreG else 255, 112 if not moreR else 255
    img = cv2.cvtColor(img, cv2.COLOR_BGR2BGRA)
    #copied code from training projector
    f, w, c = img.shape
    sbgra = (b, g, r, 1)
    sep = np.full((f, w, 4), sbgra, dtype='uint8')
    img = cv2.addWeighted(sep, intensity, img, 1.0, 0) #did not try this before
    img = cv2.cvtColor(img, cv2.COLOR_BGRA2BGR)
    return img


def saveMe(fname, img):
    cv2.imwrite(fname, img)

if __name__ == "__main__":
    while(True):
        ret, frame = cap.read()
        inin = cv2.waitKey(1)
        # capture filters
        if inin & 0xFF == ord('i'):
            if(not invert):
                invert = True
            else:
                invert = False                
        if inin & 0xFF == ord('r'):
            if(not moreblue):
                moreblue = True
            else:
                moreblue = False
        if inin & 0xFF == ord('t'):
            if(not moregreen):
                moregreen = True
            else:
                moregreen = False
        if inin & 0xFF == ord('y'):
            if(not morered):
                morered = True
            else:
                morered = False                
        if inin & 0xFF == ord('s'):
            if(not sepia):
                sepia = True
            else:
                sepia = False
        #quit
        if inin & 0xFF == ord('q'):
            break

        # process the filters here
        if(invert):
            frame = invertMe(frame)
        if(sepia):
            frame = sepiaMe(frame,moreB=moreblue,moreG=moregreen,moreR=morered)
        #show in win
        cv2.imshow('meh', frame)
