import numpy as np
import cv2
import math

def showSomething(hwnd_name, imgURI):
    img = cv2.imread(imgURI)
    cv2.imshow(hwnd_name,img)

if __name__ == "__main__":
    showSomething('umji','./assets/umji.jpg')
    cv2.waitKey(0)
    cv2.destroyAllWindows()    
