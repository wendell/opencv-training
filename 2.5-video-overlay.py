import numpy as np
import cv2
import atexit
import signal
import sys
from scipy import stats

# start capture grr
cap = cv2.VideoCapture(0)
inv = True
counter=0
DESTROYED = 0
# so i can exit anytime


def fatal():
    global DESTROYED
    if(not DESTROYED):
        DESTROYED = 1
        cap.release()
        cv2.destroyAllWindows()
        print "\nDESTROYED"


def sigint(signal, frame):
    fatal()
    sys.exit(0)


def overlay(img1, img2_o):
    global inv
    img2 = invertMe(img2_o)
    rows, cols, channels = img2.shape
    roi = img1[0:rows, 0:cols]
    img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(img2gray, 20, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)
    img1_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)
    if(inv):
        img2_fg = cv2.bitwise_and(img2_o, img2_o, mask=mask)
    else:
        img2_fg = cv2.bitwise_and(img2, img2, mask=mask)
    dst = cv2.add(img1_bg, img2_fg)
    img1[0:rows, 0:cols] = dst
    return img1


def invertMe(img):
    img = cv2.bitwise_not(img)
    # wip
    return img


def loadSomething(imgURI):
    img = cv2.imread(imgURI)
    return img


def saveMe(fname, img):
    cv2.imwrite(fname, img)

if __name__ == "__main__":
    logo = loadSomething('./assets/accenture.png')
    while(True):
        ret, frame = cap.read()
        weird = overlay(frame, logo)
        cv2.imshow('meh', weird)
        #if cv2.waitKey(1) & 0xFF == ord('s'):
            #counter += 1
            #saveMe('./saved/screenshot' + counter + '.jpg', weird)
        if cv2.waitKey(1) & 0xFF == ord('i'):
            inv = not inv
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
